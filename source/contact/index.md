---
title: Contact
date: 2016-07-04 23:05:58
---

## You are wrong about something, how do I tell you? (i.e. Where are the comments?)

It is rare that I have found comments add anything of value to a website, as such I didn't bother implement comments here. If you want to give me short form feedback please feel free to Tweet at me. If you want to give longer form feedback then I am willing (and likely happy) to add links on my posts to your blog post in response.

## Me across the internet

- {% link "Ootoovak on Twitter" http://twitter.com/ootoovak %}
- {% link "Ootoovak on Gitlab" http://gitlab.com/ootoovak %}
- {% link "Ootoovak on LinkedIn" http://nz.linkedin.com/in/ootoovak %}
