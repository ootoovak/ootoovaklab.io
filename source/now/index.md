---
title: Now
date: 2016-07-04 20:29:12
---

This is my {% link "now page" http://nownownow.com/about %}.

I am interested in Artificial Intelligence (AI) and making any level of it more accessible to others. I did some AI in university but I never got around to pursuing it much after that. So, the projects I am going to pursue over the next while will to be to with AI, as well as having a play with programming languages and tech that I am currently interested in.

The programming languages I am currently interested in and would like to have a play (in current level of interest order):
- {% link "Rust" https://www.rust-lang.org/ %}
- {% link "Elm" http://elm-lang.org/ %}
- {% link "Elixir" http://elixir-lang.org/ %}

I am also interested in distributed systems so I am interested in looking into how the following tech might work into a distributed AI system:
- {% link "MAIDSafe" https://maidsafe.net/ %}
- {% link "AWS Lambda" https://aws.amazon.com/lambda/details/ %}
- {% link "InterPlanetary File System" https://ipfs.io/ %}
- {% link "Scuttlebot" https://ssbc.github.io/scuttlebot/ %}

Finally, I have bought some "Internet of Things" tech which I will aim to incorporate into projects:
- {% link "Tessel 2" https://tessel.io/ %}
- {% link "Particle Photon" https://www.particle.io/ %}

I will update this page with links to the projects as they get started but I will also have links to them generally on this site.

One more thing that I am going to be working on is trying to find alternatives to sharing just on contemporary social media. So, if I am feeling comfortable with it I might also be trying to write a bit more in the way of personal posts as well.
