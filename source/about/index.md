---
title: About - Samson Ootoovak
date: 2016-05-13 00:35:04
---

![Samson Ootoovak (me)](/about/index/me.jpg)

## Ootoovak?

It is my last name, which is Inuk, and it is pronounced in three syllables as follows:

**Oot** *(think how a stereotypical Canadian would pronounce "out")*
**two** *(like the number 2)*
**vuck** *(like the word "duck" but starting with a v instead)*

**Oot - two - vuck**

## Who are you?

Samson Ootoovak, as mentioned above, and I am originally from Baffin Island (in northern Canada) and I am now living in New Zealand. I work professionally as a web developer but I have a general interest in STEM and art.

## What is the deal with this blog?

This blog will include personal thoughts, projects and reflections about things like programming. I use my personal time is for learning and experimentation so here is where I will aim to stretch myself, and write about the results (or even as I go). From time to time it may also include a wider range of more personal topics as well.

## Note about the blog tech

This blog is built using the {% link Hexo https://hexo.io/ %} blog framework and a heavily modified version of the {% link Flexy https://github.com/sjaakvandenberg/flexy %} theme.
