---
title: Introduction to Sawbones
date: 2014/1/7
updated: 2014/1/7
tags:
  - project
  - sawbones
---

## Reasons this project exists

There are a lot of Continuous Integration (CI) servers out there. Closed sourced, open sourced, self hosted, Software as a Service (SaaS), all with varying levels of cost, complexity, and ambition. While working on a closed sourced Rails app for a bootstrapped startup I found that the existing solutions didn't quite meet my needs. I just wanted something I could quickly set up, run all my tests automatically, and email me if there was a problem.

Of the self hosted options at the time, the projects were ether not often maintained or overly complex to set up. Not great to a time constrained startup.

On the SaaS side of things, while the CI providers generously offered free open source options the paid versions, while minimal, were even out of our reach as a cash strapped startup.

We ended up finding solution that works for now but I wanted something better. Enter, Sawbones.

## Goals of Sawbones

The Sawbones project for me is not just about the Continuous Integration, it is also about giving myself a project to learn on. There are some technologies I have been wanting to become more familiar with for a long time but up until now I have just not  had the chance. With that in mind:

### Main project goals

1. Design driven, the user experience matters.
2. Easy to set up, by focusing on doing one thing well.
3. Improve my concurrency knowledge, in this case with the use of [Celluloid](http://celluloid.io/).
4. Improve my Javascript framework knowledge, in this case with the use of [Ember](http://emberjs.com/).
5. Improve my PubSub knowledge, with something...
6. Complete an OS project to a point were it can be used.

### Likely side effects of the main project goals

1. It will be a single project per instance.
2. It will be Github web hook based first.
3. The the languages and frameworks I use to build it may change for fun.

## Potentially Asked Questions

### Sawbones?

* A CI server makes sure your app is ok, like a doctor.
* It's a simple doctor.
* Bones was already taken.
* McCoy had potential but also had potential for a legal requirement to change the name someday.
* I hadn't started watching much Dr. Who when I was thinking up a name.
* A saw makes for an easy logo.

### Celluloid, Ember, and PubSub for a CI server? That's dumb!

Ha ha, yeah I'm sure it is. It is almost certainly overkill, and might not offer any real benefits to the end user. But as I mentioned above I want to learn how they work, so why not learn on a simple project with useful goal. Just wait till I start wanting to change the backend to [Go](http://golang.org/) or [Elixir](http://elixir-lang.org/)!

### Can I trust this software?

Well as mentioned above this project will be a place for me to try out technology that is new to me so keep that in mind. That said I will keep the test coverage high, I will be using the CI server on my own projects as I go, and if I do find enough people are starting to use it besides me then I will likely stabilise it and start experimenting within other projects.

### Help get me out of here now!

Never fear! Here are some great alternatives.

#### Self Hosted

* [TravisCI](https://github.com/travis-ci/travis-ci)
* [Cruise Control](https://github.com/thoughtworks/cruisecontrol.rb)
* [CIJoe](https://github.com/defunkt/cijoe)
* [Jenkins](https://github.com/jenkinsci/jenkins.rb)
* [Integrity](https://github.com/integrity/integrity)
* [Big Tuna](https://github.com/appelier/bigtuna)

#### SaaS

* [Semaphore](https://semaphoreapp.com/)
* [Code Ship](https://www.codeship.io/)
* [Drone](https://drone.io/)
* [CircleCI](https://circleci.com/)
* [Solano Labs](https://www.solanolabs.com/)
