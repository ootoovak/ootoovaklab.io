---
title: My Ever Changing Plan To Produce
date: 2014/10/9
updated: 2014/10/9
tags:
  - personal
---

I am an avid consumer of what other people produce, I spend a lot of time reading, listening, and watching content online. As a developer I often feel bad about how little I produce and share publicly. I also tend to swing between feeling bad that I am not as an accomplished web developer as others in the field and then also feeling bad because I am not pursuing what I tell myself is my original programming passion, that of AI, robotics, and simulations. It ends up a big being a shame spiral that stops slows me down rather than motivating me in anything.

Given how much I do consume I am often in awe of those people that seem to be endlessly producing code, giving talks, writing blog posts, organising events or some combination of all at once. When I am busy at work I wish for times when I will be able to stop, and focus on my ideas for personal projects. Then when I do find myself with time off I am slow to get started, easily distracted, feeling bad if I think I am trying to learn things for professional value rather than said "passion projects" mentioned above, or lost in leaning frameworks instead of being a better coder. As a result of this I feel guilty for being distracted and not getting anything done, comparing myself to others who are seemingly so much more focused than I and thus generally being seemingly amazing people. I felt this most recently after the excellent [WDCNZ](http://wdcnz.com/) talks this year. It was a great conference but I came away a bit blue for my lack of presence in the industry or things to talk about at that level.

I have talked to some people about my lack of focus and how they get things done and have tried a few of them myself but so far nothing has seem to have stuck. So I am still on a search to find what might work best for me.

That brings me to the here and now, writing this post. At present I have some time off. Been off for three days already and have another week and a bit to go. I have been skipping between 4 code ideas, I have been doing parts of tutorials, reading, watching and listening to other peoples content (talks and frameworks) and was starting to feel the same old guilt of not getting anywhere. So, this afternoon I had this moment of insight, instead of feeling guilty for my unfocused mind I am going to try embrace it. While I will continue to consume some content to keep my mind from doing only that (and/or flicking to Facebook and Twitter every few minutes) I am going to try follow my minds current creative impulse wherever it may go without guilt. I am not going to stress about jumping around and instead have it inform me on what will keep me engaged and typing. I don't have to finish anything, I will try to share what makes sense but I will not worry too much about it being an end goal or too much about the polish of what I share.

In essence, I am going to drop the guilt that I am not finishing things and just push things to my Gitlab account often. I am not going to worry myself on whether or not a project is for professional development as a "lowly web developer" or if I am not pursuing my lofty goals of "AI/robotics/simulation" programming. It doesn't matter if I pick up a library, framework, or hand code something ridiculous. As long as most of the time my fingers are hitting keys and sharing what ever it produced for those that want to see then that is ok. In my off time my brain should have the freedom to run up and down the sand dunes of my mind with no particular direction in mind.

If anything this new technique has got me writing a blog post and if you look at the date of my last one that is an accomplishment in of itself. :D Till next time.

**End note:**

My current job at [Enspiral Dev Academy](http://devacademy.co.nz/) does mean I do help in publicly producing some pretty amazing things. That is to say new developers. I am so proud to be a part of their journey into this wonderful world we have, even if some of them do find themselves in my shoes, hindered by choice. It is not a completely terrible problem to have. :)
